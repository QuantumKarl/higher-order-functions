
srcDir = src
objDir = build
outDir = bin/gcc
appname := higher_order_functions

CXX := g++
CXXFLAGS := -g -std=gnu++14 -Wall -Wextra -Wnon-virtual-dtor  -Wold-style-cast -Wcast-align -Wunused -Woverloaded-virtual -Wpedantic -Wconversion -Wno-unused-function -O3
LDLIBS := 

STRUCTURE := $(shell find $(srcDir) -type d)
CODEFILES := $(addsuffix /*,$(STRUCTURE))
CODEFILES := $(wildcard $(CODEFILES))

srcfiles := $(filter %.cpp,$(CODEFILES))
objects  := $(patsubst %.cpp, %.o, $(srcfiles))

all: $(appname)

$(appname): $(objects)
	$(CXX) -v $(CXXFLAGS) $(LDFLAGS) -o $(appname) $(objects) $(LDLIBS)

clean:
	rm -f $(objects)

print-% : ; $(info $* is $(flavor $*) variable set to [$($*)]) @true