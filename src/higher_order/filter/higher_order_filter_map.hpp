/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_HIGHER_ORDER_FILTER_MAP_HPP )
#define D_HIGHER_ORDER_FILTER_MAP_HPP

//--------------------------------
template < typename exp_filter_t, typename exp_map_t >
class filter_map
{
  public:
    //--------------------------------
    filter_map( exp_filter_t& io_filter, exp_map_t& io_map );

    //--------------------------------
    template < typename container_t >
    void apply_to( container_t& io_container );

    //--------------------------------
    template < typename container_t >
    void apply_reversed_to( container_t& io_container );

    //--------------------------------
    template < typename func_t >
    auto filter( func_t& io_func );

    //--------------------------------
    template < typename func_t >
    auto fold( func_t& io_func );

  private:
    exp_filter_t& m_filter;
    exp_map_t&    m_map;
};

#endif