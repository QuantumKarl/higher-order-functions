/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_HIGHER_ORDER_FILTER_MAP_INL )
#define D_HIGHER_ORDER_FILTER_MAP_INL

//--------------------------------
template < typename exp_filter_t, typename exp_map_t >
filter_map< exp_filter_t, exp_map_t >::filter_map( exp_filter_t& io_filter, exp_map_t& io_map )
    : m_filter( io_filter )
    , m_map( io_map )
{
}

//--------------------------------
template < typename exp_filter_t, typename exp_map_t >
template < typename container_t >
void
    filter_map< exp_filter_t, exp_map_t >::apply_to( container_t& io_container )
{
    using iterator_t = typename container_t::iterator;
    using value_t    = typename container_t::value_type const;
    assert( io_container.size( ) > 0 );

    iterator_t itr     = std::begin( io_container );
    iterator_t end     = std::end( io_container );
    iterator_t new_end = itr;
    do
    {
        // apply filter
        bool res = m_filter( static_cast< value_t >( *itr ) );
        if ( res == true )
        {
            // apply map
            ( *itr ) = m_map( static_cast< value_t >( *itr ) );

            *new_end = std::move( *itr );
            ++new_end;
        }

        ++itr;
    } while ( itr != end );

    io_container.erase( new_end, end );
}

//--------------------------------
template < typename exp_filter_t, typename exp_map_t >
template < typename container_t >
void
    filter_map< exp_filter_t, exp_map_t >::apply_reversed_to( container_t& io_container )
{
    using iterator_t = typename container_t::reverse_iterator;
    using value_t    = typename container_t::value_type const;
    assert( io_container.size( ) > 0 );

    iterator_t itr     = std::rbegin( io_container );
    iterator_t end     = std::rend( io_container );
    iterator_t new_end = itr;
    do
    {
        // apply filter
        bool res = m_filter( static_cast< value_t >( *itr ) );
        if ( res == true )
        {
            // apply map
            ( *itr ) = m_map( static_cast< value_t >( *itr ) );

            *new_end = std::move( *itr );
            ++new_end;
        }
        ++itr;
    } while ( itr != end );

    io_container.erase( end.base( ), new_end.base( ) );
}

//--------------------------------
template < typename exp_filter_t, typename exp_map_t >
template < typename func_t >
auto
    filter_map< exp_filter_t, exp_map_t >::filter( func_t& io_func )
{
    return filter_map_filter< exp_filter_t, exp_map_t, func_t >( m_filter, m_map, io_func );
}

//--------------------------------
template < typename exp_filter_t, typename exp_map_t >
template < typename func_t >
auto
    filter_map< exp_filter_t, exp_map_t >::fold( func_t& io_func )
{
    return filter_map_fold< exp_filter_t, exp_map_t, func_t >( m_filter, m_map, io_func );
}

#endif