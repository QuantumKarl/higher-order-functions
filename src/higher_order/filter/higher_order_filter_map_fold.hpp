/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_HIGHER_ORDER_FILTER_MAP_FOLD_HPP )
#define D_HIGHER_ORDER_FILTER_MAP_FOLD_HPP

//--------------------------------
template < typename exp_filter_t, typename exp_map_t, typename exp_fold_t >
class filter_map_fold
{
  public:
    //--------------------------------
    filter_map_fold( exp_filter_t& io_filter, exp_map_t& io_map, exp_fold_t& io_fold );

    //--------------------------------
    template < typename acc_t, typename container_t >
    auto apply_to( acc_t&& i_acc, container_t& io_container );

    //--------------------------------
    template < typename container_t, typename acc_t >
    auto apply_reversed_to( container_t& io_container, acc_t&& i_acc );

  private:
    exp_filter_t& m_filter;
    exp_map_t&    m_map;
    exp_fold_t&   m_fold;
};

#endif