/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_HIGHER_ORDER_FILTER_SINGLE_INL )
#define D_HIGHER_ORDER_FILTER_SINGLE_INL

//--------------------------------
template < typename exp_filter_t >
filter_single< exp_filter_t >::filter_single( exp_filter_t& io_filter )
    : m_filter( io_filter )
{
}

//--------------------------------
template < typename exp_filter_t >
template < typename container_t >
void
    filter_single< exp_filter_t >::apply_to( container_t& io_container )
{
#if 0
	assert( io_container.size() > 0 );
	bool res = false;

	using iterator_t = container_t::iterator;
	iterator_t itr = std::begin( io_container );
	iterator_t end = std::end( io_container );
	do
	{
		res = m_filter( static_cast<container_t::value_type const>( *itr ) );
		if( res == true )
		{
			++itr;
		}
		else
		{
			itr = io_container.erase( itr );
			end = std::end( io_container );
		}
	} while( itr != end );
#else
    using iterator_t = typename container_t::iterator;
    using value_t    = typename container_t::value_type const;
    assert( io_container.size( ) > 0 );

    iterator_t itr     = std::begin( io_container );
    iterator_t end     = std::end( io_container );
    iterator_t new_end = itr;
    do
    {
        bool res = m_filter( static_cast< value_t >( *itr ) );
        if ( res == true )
        {
            *new_end = std::move( *itr );
            ++new_end;
        }
        ++itr;
    } while ( itr != end );

    io_container.erase( new_end, end );
#endif
}

//--------------------------------
template < typename exp_filter_t >
template < typename container_t >
void
    filter_single< exp_filter_t >::apply_reversed_to( container_t& io_container )
{
#if 0
	assert( io_container.size() > 0 );
	bool res = false;

	using iterator_t = container_t::reverse_iterator;
	iterator_t itr = std::rbegin( io_container );
	iterator_t end = std::rend( io_container );
	do
	{
		res = m_filter( static_cast<container_t::value_type const>( *itr ) );
		if( res == true )
		{
			std::advance( itr, 1 );
		}
		else
		{
			std::advance( itr, 1 );
			auto forward = io_container.erase( itr.base() );
			itr = static_cast<iterator_t>( forward );
			end = std::rend( io_container );
		}
	}
	while( itr != end );
#else
    using iterator_t = typename container_t::reverse_iterator;
    using value_t    = typename container_t::value_type const;
    assert( io_container.size( ) > 0 );

    iterator_t itr     = std::rbegin( io_container );
    iterator_t end     = std::rend( io_container );
    iterator_t new_end = itr;
    do
    {
        bool res = m_filter( static_cast< value_t >( *itr ) );
        if ( res == true )
        {
            *new_end = std::move( *itr );
            ++new_end;
        }
        ++itr;
    } while ( itr != end );

    io_container.erase( end.base( ), new_end.base( ) );
#endif
}

//--------------------------------
template < typename exp_filter_t >
template < typename func_t >
auto
    filter_single< exp_filter_t >::map( func_t& io_func )
{
    return filter_map< exp_filter_t, func_t >( m_filter, io_func );
}

//--------------------------------
template < typename exp_filter_t >
template < typename func_t >
auto
    filter_single< exp_filter_t >::fold( func_t& io_func )
{
    return filter_fold< exp_filter_t, func_t >( m_filter, io_func );
}

#endif