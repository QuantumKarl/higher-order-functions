/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_HIGHER_ORDER_FILTER_MAP_FILTER_HPP )
#define D_HIGHER_ORDER_FILTER_MAP_FILTER_HPP

//--------------------------------
template < typename exp_filterl_t, typename exp_map_t, typename exp_filterr_t >
filter_map_filter< exp_filterl_t, exp_map_t, exp_filterr_t >::filter_map_filter(
    exp_filterl_t& io_filterl,
    exp_map_t&     io_map,
    exp_filterr_t& io_filterr )
    : m_filterl( io_filterl )
    , m_map( io_map )
    , m_filterr( io_filterr )
{
}

//--------------------------------
template < typename exp_filterl_t, typename exp_map_t, typename exp_filterr_t >
template < typename container_t >
void
    filter_map_filter< exp_filterl_t, exp_map_t, exp_filterr_t >::apply_to(
        container_t& io_container )
{
    using iterator_t = typename container_t::iterator;
    using value_t    = typename container_t::value_type const;
    assert( io_container.size( ) > 0 );

    iterator_t itr     = std::begin( io_container );
    iterator_t end     = std::end( io_container );
    iterator_t new_end = itr;
    do
    {
        // apply filter left
        bool res = m_filterl( static_cast< value_t >( *itr ) );
        if ( res == true )
        {
            // apply map
            ( *itr ) = m_map( static_cast< value_t >( *itr ) );

            // apply filter right
            res = m_filterr( static_cast< value_t >( *itr ) );
            if ( res == true )
            {
                *new_end = std::move( *itr );
                ++new_end;
            }
        }
        ++itr;
    } while ( itr != end );

    io_container.erase( new_end, end );
}

//--------------------------------
template < typename exp_filterl_t, typename exp_map_t, typename exp_filterr_t >
template < typename container_t >
void
    filter_map_filter< exp_filterl_t, exp_map_t, exp_filterr_t >::apply_reversed_to(
        container_t& io_container )
{
    using iterator_t = typename container_t::reverse_iterator;
    using value_t    = typename container_t::value_type const;
    assert( io_container.size( ) > 0 );

    iterator_t itr     = std::rbegin( io_container );
    iterator_t end     = std::rend( io_container );
    iterator_t new_end = itr;
    do
    {
        // apply filter left
        bool res = m_filterl( static_cast< value_t >( *itr ) );
        if ( res == true )
        {
            // apply map
            ( *itr ) = m_map( static_cast< value_t >( *itr ) );

            // apply filter right
            res = m_filterr( static_cast< value_t >( *itr ) );
            if ( res == true )
            {
                *new_end = std::move( *itr );
                ++new_end;
            }
        }
        ++itr;
    } while ( itr != end );

    io_container.erase( end.base( ), new_end.base( ) );
}

#endif