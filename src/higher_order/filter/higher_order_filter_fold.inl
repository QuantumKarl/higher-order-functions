/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_HIGHER_ORDER_FILTER_FOLD_INL )
#define D_HIGHER_ORDER_FILTER_FOLD_INL

//--------------------------------
template < typename exp_filter_t, typename exp_fold_t >
filter_fold< exp_filter_t, exp_fold_t >::filter_fold( exp_filter_t& io_filter,
                                                      exp_fold_t&   io_fold )
    : m_filter( io_filter )
    , m_fold( io_fold )
{
}

//--------------------------------
template < typename exp_filter_t, typename exp_fold_t >
template < typename acc_t, typename container_t >
auto
    filter_fold< exp_filter_t, exp_fold_t >::apply_to( acc_t&&      i_acc,
                                                       container_t& io_container )
{
    using iterator_t = typename container_t::iterator;
    using value_t    = typename container_t::value_type const;
    assert( io_container.size( ) > 0 );

    acc_t acc = std::forward< acc_t >( i_acc );

    iterator_t itr = std::begin( io_container );
    iterator_t end = std::end( io_container );
    do
    {
        // apply filter
        bool res = m_filter( static_cast< value_t >( *itr ) );
        if ( res == true )
        {
            // apply fold
            acc = m_fold( static_cast< acc_t const >( acc ), static_cast< value_t >( *itr ) );
        }
        ++itr;
    } while ( itr != end );

    io_container.clear( );

    return acc;
}

//--------------------------------
template < typename exp_filter_t, typename exp_fold_t >
template < typename container_t, typename acc_t >
auto
    filter_fold< exp_filter_t, exp_fold_t >::apply_reversed_to( container_t& io_container,
                                                                acc_t&&      i_acc )
{
    using iterator_t = typename container_t::reverse_iterator;
    using value_t    = typename container_t::value_type const;
    assert( io_container.size( ) > 0 );

    acc_t acc = std::forward< acc_t >( i_acc );

    iterator_t itr = std::rbegin( io_container );
    iterator_t end = std::rend( io_container );
    do
    {
        // apply filter
        bool res = m_filter( static_cast< value_t >( *itr ) );
        if ( res == true )
        {
            // apply fold
            acc = m_fold( static_cast< value_t >( *itr ), static_cast< acc_t const >( acc ) );
        }
        ++itr;
    } while ( itr != end );

    io_container.clear( );

    return acc;
}

#endif