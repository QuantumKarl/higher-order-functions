/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_HIGHER_ORDER_MAP_SINGLE_INL )
#define D_HIGHER_ORDER_MAP_SINGLE_INL

//--------------------------------
template < typename exp_map_t >
map_single< exp_map_t >::map_single( exp_map_t& io_map )
    : m_map( io_map )
{
}

//--------------------------------
template < typename exp_map_t >
template < typename container_t >
void
    map_single< exp_map_t >::apply_to( container_t& io_container )
{
    using iterator_t = typename container_t::iterator;
    using value_t    = typename container_t::value_type const;
    assert( io_container.size( ) > 0 );

    iterator_t itr = std::begin( io_container );
    iterator_t end = std::end( io_container );
    do
    {
        // apply map
        ( *itr ) = m_map( static_cast< value_t >( *itr ) );
        ++itr;
    } while ( itr != end );
}

//--------------------------------
template < typename exp_map_t >
template < typename container_t >
void
    map_single< exp_map_t >::apply_reversed_to( container_t& io_container )
{
    using iterator_t = typename container_t::reverse_iterator;
    using value_t    = typename container_t::value_type const;
    assert( io_container.size( ) > 0 );

    iterator_t itr = std::rbegin( io_container );
    iterator_t end = std::rend( io_container );
    do
    {
        // apply map
        ( *itr ) = m_map( static_cast< value_t >( *itr ) );
        ++itr;
    } while ( itr != end );
}

//--------------------------------
template < typename exp_map_t >
template < typename func_t >
auto
    map_single< exp_map_t >::filter( func_t& io_func )
{
    return map_filter< exp_map_t, func_t >( m_map, io_func );
}

//--------------------------------
template < typename exp_map_t >
template < typename func_t >
auto
    map_single< exp_map_t >::fold( func_t& io_func )
{
    return map_fold< exp_map_t, func_t >( m_map, io_func );
}

#endif