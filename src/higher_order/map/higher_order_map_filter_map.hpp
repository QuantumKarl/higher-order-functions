/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_HIGHER_ORDER_MAP_FILTER_MAP_HPP )
#define D_HIGHER_ORDER_MAP_FILTER_MAP_HPP

//--------------------------------
template < typename exp_mapl_t, typename exp_filter_t, typename exp_mapr_t >
class map_filter_map
{
  public:
    //--------------------------------
    map_filter_map( exp_mapl_t& io_mapl, exp_filter_t& io_filter, exp_mapr_t& io_mapr );

    //--------------------------------
    template < typename container_t >
    void apply_to( container_t& io_container );

    //--------------------------------
    template < typename container_t >
    void apply_reversed_to( container_t& io_container );

  private:
    exp_mapl_t&   m_mapl;
    exp_filter_t& m_filter;
    exp_mapr_t&   m_mapr;
};

#endif