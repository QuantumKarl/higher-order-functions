/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_HIGHER_ORDER_HPP )
#define D_HIGHER_ORDER_HPP

#include <cassert>
#include <iterator>
#include <type_traits>
#include <utility>

//------------------------
/*
	["map"]
	["map","filter"]
	["map","fold"]
	["map", "filter", "map"]
	["map","filter","fold"]

	["filter"]
	["filter","map"]
	["filter","fold"]
	["filter","map","fold"] 
	["filter","map","filter"]

	["fold"]
*/

namespace detail_higher_order
{
//----------------
// map hpp
#include "map/higher_order_map_filter.hpp"
#include "map/higher_order_map_filter_fold.hpp"
#include "map/higher_order_map_filter_map.hpp"
#include "map/higher_order_map_fold.hpp"
#include "map/higher_order_map_single.hpp"

//----------------
// filter hpp
#include "filter/higher_order_filter_fold.hpp"
#include "filter/higher_order_filter_map.hpp"
#include "filter/higher_order_filter_map_filter.hpp"
#include "filter/higher_order_filter_map_fold.hpp"
#include "filter/higher_order_filter_single.hpp"

//----------------
// fold hpp
#include "fold/higher_order_fold_single.hpp"

//----------------
// map inl
#include "map/higher_order_map_filter.inl"
#include "map/higher_order_map_filter_fold.inl"
#include "map/higher_order_map_filter_map.inl"
#include "map/higher_order_map_fold.inl"
#include "map/higher_order_map_single.inl"

//----------------
// filter inl
#include "filter/higher_order_filter_fold.inl"
#include "filter/higher_order_filter_map.inl"
#include "filter/higher_order_filter_map_filter.inl"
#include "filter/higher_order_filter_map_fold.inl"
#include "filter/higher_order_filter_single.inl"

//----------------
// fold inl
#include "fold/higher_order_fold_single.inl"

} // end of namespace

//--------------------------------
template < typename func_t >
auto
    map( func_t& io_func )
{
    using namespace detail_higher_order;
    return map_single< func_t >( io_func );
}

//--------------------------------
template < typename func_t >
auto
    filter( func_t& io_func )
{
    using namespace detail_higher_order;
    return filter_single< func_t >( io_func );
}

//--------------------------------
template < typename func_t >
auto
    fold( func_t& io_func )
{
    using namespace detail_higher_order;
    return fold_single< func_t >( io_func );
}

#endif