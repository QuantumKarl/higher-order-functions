/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#include "higher_order/higher_order.hpp"
#include "tests/benchmark.hpp"
#include "tests/tests.hpp"
#include <cmath>

#include "tests\writeToCSV.hpp"

// https://hackage.haskell.org/package/base-4.9.1.0/docs/Data-List.html

// TODO: investigate changing fold to accumulate in the accumulator
// TODO: add comments
// TODO: compare asm from new and std

// large test
#if 8
constexpr size_t num_tests = 8; // 1.0GB of doubles
#else
// small test
constexpr size_t num_tests = 1;
#endif

//--------------------------------
/* calculates the percentage progress
	per step of the test.
calcSteps n = map (/s) p2
		where
			p2 = map (2**) [1..n]
			s = sum p2
*/
std::vector< double >
    calcSteps( size_t const n )
{
    std::vector< double > res;
    res.reserve( n );

    double sum  = 0.0;
    double temp = 0.0;
    for ( size_t i = 0; i < n; ++i )
    {
        temp = pow( 2, ( i + 1 ) );
        sum += temp;
        res.push_back( temp );
    }

    for ( auto& e : res )
    {
        e = e / sum;
    }

    return res; // RVO
}

//--------------------------------
int
    main( )
{
    Tester t( 100 );
    t.test_all( );

    std::vector< double > steps    = calcSteps( num_tests );
    double                progress = 0.0;

    std::vector< std::string > results;
    for ( size_t i = 0; i < num_tests; ++i )
    {
        size_t    count = static_cast< size_t >( pow( 2, 19 + i ) );
        Benchmark b( count );
        b.run_all( results );
        progress += steps[i];
        std::cout << ( progress * 100.0 ) << "%" << std::endl;
        results.push_back( "" );
    }

    //---- output to screen ----
    for ( auto e : results )
    {
        std::cout << e << std::endl;
    }

    //---- write to file ----
    writeToCSV( results, "results" );

    return 0;
}