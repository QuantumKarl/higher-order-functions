/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_TESTER_HPP )
#define D_TESTER_HPP

#include <iostream>
#include <random>
#include <vector>

class Tester
{
  public:
    Tester( size_t const i_data_count );

    void test_all( );

  private:
    using cont_t = std::vector< int >;

    cont_t generate_source_data( );

    void map_simple( );
    void map_constructed( );
    void map_static( );
    void map_manual_acc( );
    void fold_manual( );
    void fold_constructed( );
    void fold_map_constructed( );
    void filter_manual( );
    void filter_constructed( );
    void map_filter_constructed( );
    void map_filter_map_constructed( );
    void map_filter_fold_constructed( );
    void filter_map_constructed( );
    void filter_fold_constructed( );
    void filter_map_filter_constructed( );
    void filter_map_fold_constructed( );
    void filtering( );

    std::default_random_engine           m_engine;
    std::uniform_int_distribution< int > m_dist;
    size_t                               m_data_count;
};

#endif