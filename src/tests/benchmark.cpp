/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#include "benchmark.hpp"

#include "..\higher_order\higher_order.hpp"

#include <iostream>
#include <numeric>
#include <sstream>

namespace detail
{
//--------------------------------
/* operator +=
	scala application of +=
*/
template < typename T >
void
    operator+=( std::vector< T >& io_lhs, std::vector< T >&& i_rhs )
{
    assert( io_lhs.size( ) == i_rhs.size( ) );
    for ( size_t i = 0, c = io_lhs.size( ); i < c; ++i )
    {
        io_lhs[i] += i_rhs[i];
    }
}

//--------------------------------
/* operator /=
	scala application of /=
*/
template < typename T >
void
    operator/=( std::vector< T >& io_lhs, T const& i_scala )
{
    for ( size_t i = 0, c = io_lhs.size( ); i < c; ++i )
    {
        io_lhs[i] = T( io_lhs[i] / i_scala );
    }
}
}

//--------------------------------
Benchmark::Benchmark( size_t const i_data_count )
    : m_data_count( i_data_count )
{
}

//--------------------------------
/* run()
*/
void
    Benchmark::run_all( std::vector< std::string >& i_results )
{
    using namespace detail;
#if defined( D_NEW_FIRST ) && D_NEW_FIRST == 1
    std::vector< Benchmark::time_t > new_results = run_new( );
    std::vector< Benchmark::time_t > std_results = run_std( );

    new_results += run_new( ); // temporary destroyed
    std_results += run_std( ); // temporary possibly reused
#else
    std::vector< Benchmark::time_t > std_results = run_std( );
    std::vector< Benchmark::time_t > new_results = run_new( );

    std_results += run_std( ); // temporary destroyed
    new_results += run_new( ); // temporary possibly reused
#endif

    Benchmark::time_t constexpr count( 2.0 );
    new_results /= count;
    std_results /= count;

    std::stringstream ss;
    ss << m_data_count;
    i_results.push_back( ss.str( ) );
    ss.clear( );
    ss.str( "" );
    {
        std::string& row = i_results.back( );
        for ( size_t i = 0, c = toUType( Benchmark::eID::count ); i < c; ++i )
        {
            row.append( ", " );
            row.append( m_testNames[i] );
        }
    }

    i_results.push_back( "new" );
    {
        std::string& row = i_results.back( );
        for ( size_t i = 0, c = toUType( Benchmark::eID::count ); i < c; ++i )
        {
            ss.clear( );
            ss.str( "" );
            ss << new_results[i].count( );
            row.append( ", " );
            row.append( ss.str( ) );
        }
    }

    i_results.push_back( "std" );
    {
        std::string& row = i_results.back( );
        for ( size_t i = 0, c = toUType( Benchmark::eID::count ); i < c; ++i )
        {
            ss.clear( );
            ss.str( "" );
            ss << std_results[i].count( );
            row.append( ", " );
            row.append( ss.str( ) );
        }
    }
}

//--------------------------------
std::vector< Benchmark::time_t >
    Benchmark::run_new( )
{
    using namespace detail;
    std::vector< Benchmark::time_t > times;
    times.resize( toUType( Benchmark::eID::count ) );

    times[toUType( Benchmark::eID::map_filter )]        = map_filter_new( );
    times[toUType( Benchmark::eID::map_filter_map )]    = map_filter_map_new( );
    times[toUType( Benchmark::eID::map_filter_fold )]   = map_filter_fold_new( );
    times[toUType( Benchmark::eID::filter_map )]        = filter_map_new( );
    times[toUType( Benchmark::eID::filter_fold )]       = filter_fold_new( );
    times[toUType( Benchmark::eID::filter_map_filter )] = filter_map_filter_new( );
    times[toUType( Benchmark::eID::filter_map_fold )]   = filter_map_fold_new( );

    return times;
}

//--------------------------------
std::vector< Benchmark::time_t >
    Benchmark::run_std( )
{
    using namespace detail;
    std::vector< Benchmark::time_t > times;
    times.resize( toUType( Benchmark::eID::count ) );

    times[toUType( Benchmark::eID::map_filter )]        = map_filter_std( );
    times[toUType( Benchmark::eID::map_filter_map )]    = map_filter_map_std( );
    times[toUType( Benchmark::eID::map_filter_fold )]   = map_filter_fold_std( );
    times[toUType( Benchmark::eID::filter_map )]        = filter_map_std( );
    times[toUType( Benchmark::eID::filter_fold )]       = filter_fold_std( );
    times[toUType( Benchmark::eID::filter_map_filter )] = filter_map_filter_std( );
    times[toUType( Benchmark::eID::filter_map_fold )]   = filter_map_fold_std( );

    return times;
}

//--------------------------------
Benchmark::time_t
    Benchmark::map_filter_new( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );

    auto l1 = []( auto i ) { return i * 3.0; };
    auto f1 = []( auto arg ) { return arg >= 0.0; };

    map( l1 ).filter( f1 ).apply_to( v1 );

    t.End( );
    t.CalcDuration( );
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::map_filter_std( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );
    std::vector< double > out;
    out.resize( v1.size( ) );

    auto l1 = []( auto i ) { return i * 3.0; };
    auto f1 = []( auto arg ) { return arg <= 0.0; };

    // apply map
    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( out ), l1 );

    // apply filter
    auto newEnd = std::remove_if( std::begin( out ), std::end( out ), f1 );
    out.resize( std::distance( std::begin( out ), newEnd ) );

    t.End( );
    t.CalcDuration( );
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::map_filter_map_new( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );

    auto l1 = []( auto i ) { return i + 1.0; };
    auto f1 = []( auto arg ) { return arg >= 0.0; };
    auto l2 = []( auto i ) { return i - 1.0; };

    map( l1 ).filter( f1 ).map( l2 ).apply_to( v1 );

    t.End( );
    t.CalcDuration( );
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::map_filter_map_std( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );
    std::vector< double > out;
    out.resize( v1.size( ) );

    auto l1 = []( auto i ) { return i + 1.0; };
    auto f1 = []( auto arg ) { return arg <= 0.0; };
    auto l2 = []( auto i ) { return i - 1.0; };

    // apply map
    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( out ), l1 );

    // apply filter
    auto newEnd = std::remove_if( std::begin( out ), std::end( out ), f1 );
    out.resize( std::distance( std::begin( out ), newEnd ) );

    // apply map
    v1.resize( out.size( ) );
    std::transform( std::begin( out ), std::end( out ), std::begin( v1 ), l2 );

    t.End( );
    t.CalcDuration( );
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::map_filter_fold_new( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );

    auto l1      = []( auto i ) { return i + 1.0; };
    auto f0      = []( auto arg ) { return arg >= 0.0; };
    auto foldr_f = []( auto acc, auto arg ) { return acc - arg; };

    auto res = map( l1 ).filter( f0 ).fold( foldr_f ).apply_to( 0.0, v1 );

    t.End( );
    t.CalcDuration( );

    // make sure the compiler wont optimise away res
    if ( res >= 0.0 )
    {
        std::cout << res << std::endl;
    }
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::map_filter_fold_std( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );
    std::vector< double > out;
    out.resize( v1.size( ) );

    auto l1      = []( auto i ) { return i + 1.0; };
    auto f0      = []( auto arg ) { return arg <= 0.0; };
    auto foldr_f = []( auto acc, auto arg ) { return acc - arg; };

    // apply map
    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( out ), l1 );

    // apply filter
    auto newEnd = std::remove_if( std::begin( out ), std::end( out ), f0 );
    out.resize( std::distance( std::begin( out ), newEnd ) );

    // apply fold
    auto res = std::accumulate( std::begin( out ), std::end( out ), 0.0, foldr_f );

    t.End( );
    t.CalcDuration( );

    // make sure the compiler wont optimise away res
    if ( res >= 0.0 )
    {
        std::cout << res << std::endl;
    }
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::filter_map_new( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );

    auto l1 = []( auto i ) { return i + 1.0; };
    auto f1 = []( auto arg ) { return arg >= 0.0; };

    filter( f1 ).map( l1 ).apply_to( v1 );

    t.End( );
    t.CalcDuration( );
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::filter_map_std( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );
    std::vector< double > out;

    auto l1 = []( auto i ) { return i + 1; };
    auto f1 = []( auto arg ) { return arg <= 0.0; };

    // apply filter
    auto newEnd = std::remove_if( std::begin( v1 ), std::end( v1 ), f1 );
    v1.resize( std::distance( std::begin( v1 ), newEnd ) );

    // apply map
    out.resize( v1.size( ) );
    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( out ), l1 );

    t.End( );
    t.CalcDuration( );
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::filter_fold_new( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );

    auto f1      = []( auto arg ) { return arg >= 0.0; };
    auto foldr_f = []( auto acc, auto arg ) { return acc - arg; };

    auto res = filter( f1 ).fold( foldr_f ).apply_to( 7.0, v1 );

    t.End( );
    t.CalcDuration( );

    // make sure the compiler wont optimise away res
    if ( res >= 0.0 )
    {
        std::cout << res << std::endl;
    }
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::filter_fold_std( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );

    auto f1      = []( auto arg ) { return arg <= 0.0; };
    auto foldr_f = []( auto acc, auto arg ) { return acc - arg; };

    // filter
    auto newEnd = std::remove_if( std::begin( v1 ), std::end( v1 ), f1 );
    v1.resize( std::distance( std::begin( v1 ), newEnd ) );

    // fold
    auto res = std::accumulate( std::begin( v1 ), std::end( v1 ), 7.0, foldr_f );

    t.End( );
    t.CalcDuration( );

    // make sure the compiler wont optimise away res
    if ( res >= 0.0 )
    {
        std::cout << res << std::endl;
    }
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::filter_map_filter_new( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );

    auto l1 = []( auto i ) { return i + 1.0; };
    auto f1 = []( auto arg ) { return arg >= 0.0; };

    filter( f1 ).map( l1 ).filter( f1 ).apply_to( v1 );

    t.End( );
    t.CalcDuration( );
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::filter_map_filter_std( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );
    std::vector< double > out;

    auto l1  = []( auto i ) { return i + 1.0; };
    auto f1_ = []( auto arg ) { return arg <= 0.0; };

    // filter
    {
        auto newEnd = std::remove_if( std::begin( v1 ), std::end( v1 ), f1_ );
        v1.resize( std::distance( std::begin( v1 ), newEnd ) );
    }

    // map
    out.resize( v1.size( ) );
    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( out ), l1 );

    // filter
    {
        auto newEnd = std::remove_if( std::begin( out ), std::end( out ), f1_ );
        out.resize( std::distance( std::begin( out ), newEnd ) );
    }

    t.End( );
    t.CalcDuration( );
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::filter_map_fold_new( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );

    auto l1       = []( auto i ) { return i + 1.0; };
    auto filter_f = []( auto arg ) { return arg >= 0.0; };
    auto foldr_f  = []( auto acc, auto arg ) { return acc - arg; };

    auto res = filter( filter_f ).map( l1 ).fold( foldr_f ).apply_to( 0.0, v1 );

    t.End( );
    t.CalcDuration( );

    // make sure the compiler wont optimise away res
    if ( res >= 0.0 )
    {
        std::cout << res << std::endl;
    }
    return t.GetDuration( );
}

//--------------------------------
Benchmark::time_t
    Benchmark::filter_map_fold_std( ) const
{
    Timer_t t;
    auto    v1 = init_data( );
    t.Start( );
    std::vector< double > out;

    auto l1       = []( auto i ) { return i + 1.0; };
    auto filter_t = []( auto arg ) { return arg <= 0.0; };
    auto foldr_f  = []( auto acc, auto arg ) { return acc - arg; };

    // filter
    auto newEnd = std::remove_if( std::begin( v1 ), std::end( v1 ), filter_t );
    v1.resize( std::distance( std::begin( v1 ), newEnd ) );

    // map
    out.resize( v1.size( ) );
    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( out ), l1 );

    // fold
    auto res = std::accumulate( std::begin( out ), std::end( out ), 0.0, foldr_f );

    t.End( );
    t.CalcDuration( );

    // make sure the compiler wont optimise away res
    if ( res >= 0.0 )
    {
        std::cout << res << std::endl;
    }
    return t.GetDuration( );
}

//--------------------------------
std::default_random_engine
    Benchmark::init_generator( ) const
{
    // NOTE: constant seed value to ensure fair tests
    size_t constexpr seed_value = 1024 * 1337;
    return std::default_random_engine( seed_value ); // RVO
}

//--------------------------------
std::uniform_real_distribution< double >
    Benchmark::init_distribution( ) const
{
    return std::uniform_real_distribution< double >(
        -static_cast< double >( m_data_count ),
        static_cast< double >( 2 * m_data_count ) ); // RVO
}

//--------------------------------
/* init_data()

	NOTE: indeed since the seed for the random generator is the same every time this will
	generate the same numbers every time, however a new vector is created to keep the tests fair.
	Each iteration is reading freshly allocated memory, that way the cache behavior should be most similar.
*/
std::vector< double >
    Benchmark::init_data( ) const
{
    auto gen  = init_generator( );
    auto dist = init_distribution( );

    std::vector< double > data;
    data.reserve( m_data_count );
    for ( size_t i = 0; i < m_data_count; ++i )
    {
        data.emplace_back( dist( gen ) );
    }

    return data; // RVO
}

#undef D_NEW_FIRST