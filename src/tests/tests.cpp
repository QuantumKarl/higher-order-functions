/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#include "tests.hpp"
#include "../higher_order/higher_order.hpp"

#include <algorithm>
#include <iterator>
#include <numeric>

#include <array>

namespace
{
//--------------------------------
template < class... args_t >
std::ostream&
    operator<<( std::ostream& o_stream, std::vector< args_t... > const& i_container )
{
    itrator_print( std::begin( i_container ), std::end( i_container ), o_stream );
    return o_stream;
}

//--------------------------------
/* itrator_print
*/
template < typename itr_t >
void
    itrator_print( itr_t i_first, itr_t i_last, std::ostream& o_stream )
{
    auto const count = std::distance( i_first, i_last );
    o_stream << '[';
    if ( count > 0 )
    {
        o_stream << ( *i_first );

        if ( count > 1 )
        {
            auto cur = i_first; // we have already done the first element (above)
            auto end = i_last;
            ++cur;
            do
            {
                o_stream << ',' << *cur;
            } while ( ++cur != end );
        }
    }
    o_stream << "]\n";
}

//--------------------------------
/* global operator == for vectors
*/
template < typename T >
bool
    operator==( std::vector< T > const& i_lhs, std::vector< T > const& i_rhs )
{
    if ( i_lhs.size( ) == i_rhs.size( ) )
    {
        for ( size_t i = 0, c = i_lhs.size( ); i < c; ++i )
        {
            if ( i_lhs[i] != i_rhs[i] )
            {
                return false;
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

//--------------------------------
void
    test_assert( bool const i_bool )
{
    if ( i_bool == false )
    {
        assert( false );
        std::cout << "failed test\n" << ( *( size_t volatile* )nullptr );
    }
}

} // namespace

//----------------------------------------------------------------

namespace
{
template < typename T >
union get_bytes
{
    get_bytes( T i_data )
        : data( i_data )
    {
    }

    T                               data;
    std::array< char, sizeof( T ) > bytes;
};

} // namespace

//--------------------------------
Tester::Tester( size_t const i_data_count )
    : m_engine( )
    , m_dist( -static_cast< cont_t::value_type >( i_data_count ),
              static_cast< cont_t::value_type >( 2 * i_data_count ) )
    , m_data_count( i_data_count )
{
    get_bytes< size_t > const address = ( size_t )( &address );
    std::seed_seq             seed( std::begin( address.bytes ), std::end( address.bytes ) );
    m_engine.seed( seed );
}

//--------------------------------
Tester::cont_t
    Tester::generate_source_data( )
{
    cont_t data;
    data.reserve( m_data_count );
    for ( size_t i = 0; i < m_data_count; ++i )
    {
        data.push_back( m_dist( m_engine ) );
    }

    return data; // RVO
}

//--------------------------------
void
    Tester::map_simple( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1.size( ) );
    cont_t answer2( v2.size( ) );

    auto l1 = []( auto i ) { return i * 2; };

    using namespace detail_higher_order;
    auto stage1 = map_single< decltype( l1 ) >( l1 );
    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( answer1 ), l1 );
    std::transform( std::rbegin( v2 ), std::rend( v2 ), std::rbegin( answer2 ), l1 );

    stage1.apply_to( v1 );
    stage1.apply_reversed_to( v2 );

    test_assert( v1 == v2 && v1 == answer1 && v2 == answer2 );
}

//--------------------------------
void
    Tester::map_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1.size( ) );
    cont_t answer2( v2.size( ) );

    auto l1 = []( auto i ) { return i * 2; };
    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( answer1 ), l1 );
    std::transform( std::rbegin( v2 ), std::rend( v2 ), std::rbegin( answer2 ), l1 );

    map( l1 ).apply_to( v1 );
    map( l1 ).apply_reversed_to( v2 );

    test_assert( v1 == v2 && v1 == answer1 && v2 == answer2 );
}

//--------------------------------
namespace
{
int
    static_function( int i )
{
    return i * 3;
}

} // namespace

//--------------------------------
void
    Tester::map_static( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1.size( ) );
    cont_t answer2( v2.size( ) );

    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( answer1 ), static_function );
    std::transform(
        std::rbegin( v2 ), std::rend( v2 ), std::rbegin( answer2 ), static_function );
    map( static_function ).apply_to( v1 );
    map( static_function ).apply_reversed_to( v2 );

    test_assert( v1 == v2 && v1 == answer1 && v2 == answer2 );
}

//--------------------------------
namespace
{
class thing
{
  public:
    thing( )
        : acc( 0 )
    {
    }

    int acc;

    int
        operator( )( int i )
    {
        acc += i;
        return i * 3;
    }
};

} // namespace

//--------------------------------
void
    Tester::map_manual_acc( )
{
    thing instance1;
    thing instance2;
    thing instance3;
    thing instance4;

    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1.size( ) );
    cont_t answer2( v2.size( ) );

    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( answer1 ), instance1 );
    std::transform( std::rbegin( v2 ), std::rend( v2 ), std::rbegin( answer2 ), instance2 );
    map( instance3 ).apply_to( v1 );
    map( instance4 ).apply_reversed_to( v2 );

    test_assert( v1 == v2 && v1 == answer1 && v2 == answer2 );
    test_assert( instance1.acc == instance2.acc && instance3.acc == instance4.acc );
    // NOTE: instance1 and instance2 are passed by value so the internal acc is lost
}

//--------------------------------
void
    Tester::fold_manual( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );

    auto f1 = []( auto acc, auto arg ) { return acc - arg; };
    auto f2 = []( auto arg, auto acc ) { return arg - acc; };

    auto f1_ = []( auto acc, auto arg ) { return acc - arg; };
    auto f2_ = []( auto arg, auto acc ) { return acc - arg; };

    auto res1 = std::accumulate( std::begin( v1 ), std::end( v1 ), 0, f1_ );
    auto res2 = std::accumulate( std::rbegin( v2 ), std::rend( v2 ), 0, f2_ );

    using namespace detail_higher_order;
    auto res3 = fold_single< decltype( f1 ) >( f1 ).apply_to( 0, v1 );          // foldr
    auto res4 = fold_single< decltype( f2 ) >( f2 ).apply_reversed_to( v2, 0 ); // foldl

    test_assert( v1 == v2 && res1 == res3 && res2 == res4 );
}

//--------------------------------
void
    Tester::fold_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );

    auto f1 = []( auto acc, auto arg ) { return acc - arg; };
    auto f2 = []( auto arg, auto acc ) { return arg - acc; };

    auto f1_ = []( auto acc, auto arg ) { return acc - arg; };
    auto f2_ = []( auto arg, auto acc ) { return acc - arg; };

    auto res1 = std::accumulate( std::begin( v1 ), std::end( v1 ), 0, f1_ );
    auto res2 = std::accumulate( std::rbegin( v2 ), std::rend( v2 ), 0, f2_ );

    auto res3 = fold( f1 ).apply_to( 0, v1 );          // foldr
    auto res4 = fold( f2 ).apply_reversed_to( v2, 0 ); // foldl

    test_assert( v1 == v2 && res1 == res3 && res2 == res4 );
}

//--------------------------------
void
    Tester::fold_map_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1.size( ) );
    cont_t answer2( v2.size( ) );

    auto l1 = []( auto arg ) { return arg * 2; };

    auto f1 = []( auto acc, auto arg ) { return acc - arg; };
    auto f2 = []( auto arg, auto acc ) { return arg - acc; };

    auto f1_ = []( auto acc, auto arg ) { return acc - arg; };
    auto f2_ = []( auto arg, auto acc ) { return acc - arg; };

    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( answer1 ), l1 );
    std::transform( std::rbegin( v2 ), std::rend( v2 ), std::rbegin( answer2 ), l1 );

    test_assert( answer1 == answer2 );

    auto res1 = std::accumulate( std::begin( answer1 ), std::end( answer1 ), 0, f1_ );
    auto res2 = std::accumulate( std::rbegin( answer2 ), std::rend( answer2 ), 0, f2_ );

    auto res3 = map( l1 ).fold( f1 ).apply_to( 0, v1 );
    auto res4 = map( l1 ).fold( f2 ).apply_reversed_to( v2, 0 );

    test_assert( v1 == v2 && res1 == res3 && res2 == res4 );
}

//--------------------------------
void
    Tester::filter_manual( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1 );
    cont_t answer2( v2 );

    auto f1 = []( auto arg ) { return ( arg % 2 ) == 0; };

    auto f1_ = []( auto arg ) { return ( ( arg % 2 ) == 0 ) == false; };

    {
        auto newEnd = std::remove_if( std::begin( answer1 ), std::end( answer1 ), f1_ );
        answer1.resize( std::distance( std::begin( answer1 ), newEnd ) );
    }
    {
        auto newEnd = std::remove_if( std::rbegin( answer2 ), std::rend( answer2 ), f1_ );
        answer2.erase( std::begin( answer2 ), newEnd.base( ) );
    }

    using namespace detail_higher_order;
    filter_single< decltype( f1 ) >( f1 ).apply_to( v1 );
    filter_single< decltype( f1 ) >( f1 ).apply_reversed_to( v2 );

    test_assert( v1 == answer1 && v2 == answer2 );
}

//--------------------------------
void
    Tester::filter_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1 );
    cont_t answer2( v2 );

    auto f1 = []( auto arg ) { return ( arg % 2 ) == 0; };

    auto f1_ = []( auto arg ) { return ( ( arg % 2 ) == 0 ) == false; };

    {
        auto newEnd = std::remove_if( std::begin( answer1 ), std::end( answer1 ), f1_ );
        answer1.resize( std::distance( std::begin( answer1 ), newEnd ) );
    }
    {
        auto newEnd = std::remove_if( std::rbegin( answer2 ), std::rend( answer2 ), f1_ );
        answer2.erase( std::begin( answer2 ), newEnd.base( ) );
    }

    filter( f1 ).apply_to( v1 );
    filter( f1 ).apply_reversed_to( v2 );

    test_assert( v1 == answer1 && v2 == answer2 );
}

//--------------------------------
void
    Tester::map_filter_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1 );
    cont_t answer2( v2 );

    auto l1 = []( auto i ) { return i * 3; };

    auto f1  = []( auto arg ) { return ( arg % 2 ) == 0; };
    auto f1_ = []( auto arg ) { return ( ( arg % 2 ) == 0 ) == false; };

    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( answer1 ), l1 );
    std::transform( std::rbegin( v2 ), std::rend( v2 ), std::rbegin( answer2 ), l1 );

    test_assert( answer1 == answer2 );

    {
        auto newEnd = std::remove_if( std::begin( answer1 ), std::end( answer1 ), f1_ );
        answer1.resize( std::distance( std::begin( answer1 ), newEnd ) );
    }
    {
        auto newEnd = std::remove_if( std::rbegin( answer2 ), std::rend( answer2 ), f1_ );
        answer2.erase( std::begin( answer2 ), newEnd.base( ) );
    }

    map( l1 ).filter( f1 ).apply_to( v1 );
    map( l1 ).filter( f1 ).apply_to( v2 );

    test_assert( v1 == answer1 && v2 == answer2 );
}

//--------------------------------
void
    Tester::map_filter_map_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1 );
    cont_t answer2( v2 );
    cont_t answer3;
    cont_t answer4;

    auto l1  = []( auto i ) { return i + 1; };
    auto f1  = []( auto arg ) { return ( arg % 2 ) == 0; };
    auto f1_ = []( auto arg ) { return ( ( arg % 2 ) == 0 ) == false; };
    auto l2  = []( auto i ) { return i - 1; };

    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( answer1 ), l1 );
    std::transform( std::rbegin( v2 ), std::rend( v2 ), std::rbegin( answer2 ), l1 );

    test_assert( answer1 == answer2 );

    {
        auto newEnd = std::remove_if( std::begin( answer1 ), std::end( answer1 ), f1_ );
        answer1.resize( std::distance( std::begin( answer1 ), newEnd ) );
    }
    {
        auto newEnd = std::remove_if( std::rbegin( answer2 ), std::rend( answer2 ), f1_ );
        answer2.erase( std::begin( answer2 ), newEnd.base( ) );
    }

    answer3.resize( answer1.size( ) );
    answer4.resize( answer2.size( ) );

    std::transform( std::begin( answer1 ), std::end( answer1 ), std::begin( answer3 ), l2 );
    std::transform( std::rbegin( answer2 ), std::rend( answer2 ), std::rbegin( answer4 ), l2 );

    map( l1 ).filter( f1 ).map( l2 ).apply_to( v1 );
    map( l1 ).filter( f1 ).map( l2 ).apply_reversed_to( v2 );

    test_assert( v1 == answer3 && v2 == answer4 );
}

//--------------------------------
void
    Tester::map_filter_fold_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1 );
    cont_t answer2( v2 );
    cont_t answer3;
    cont_t answer4;

    auto l1  = []( auto i ) { return i + 1; };
    auto f0  = []( auto arg ) { return ( arg % 2 ) == 0; };
    auto f0_ = []( auto arg ) { return ( ( arg % 2 ) == 0 ) == false; };

    auto foldr_f = []( auto acc, auto arg ) { return acc - arg; };
    auto foldl_f = []( auto arg, auto acc ) { return arg - acc; };

    auto foldr_f_ = []( auto acc, auto arg ) { return acc - arg; };
    auto foldl_f_ = []( auto arg, auto acc ) { return acc - arg; };

    std::transform( std::begin( v1 ), std::end( v1 ), std::begin( answer1 ), l1 );
    std::transform( std::rbegin( v2 ), std::rend( v2 ), std::rbegin( answer2 ), l1 );

    test_assert( answer1 == answer2 );

    {
        auto newEnd = std::remove_if( std::begin( answer1 ), std::end( answer1 ), f0_ );
        answer1.resize( std::distance( std::begin( answer1 ), newEnd ) );
    }
    {
        auto newEnd = std::remove_if( std::rbegin( answer2 ), std::rend( answer2 ), f0_ );
        answer2.erase( std::begin( answer2 ), newEnd.base( ) );
    }

    auto res1 = std::accumulate( std::begin( answer1 ), std::end( answer1 ), 0, foldr_f_ );
    auto res2 = std::accumulate( std::rbegin( answer2 ), std::rend( answer2 ), 0, foldl_f_ );

    auto res3 = map( l1 ).filter( f0 ).fold( foldr_f ).apply_to( 0, v1 );
    auto res4 = map( l1 ).filter( f0 ).fold( foldl_f ).apply_reversed_to( v2, 0 );

    test_assert( res1 == res3 && res2 == res4 );
}

//--------------------------------
void
    Tester::filter_map_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1 );
    cont_t answer2( v2 );
    cont_t answer3;
    cont_t answer4;

    auto l1 = []( auto i ) { return i + 1; };

    auto f1  = []( auto arg ) { return ( arg % 2 ) == 0; };
    auto f1_ = []( auto arg ) { return ( ( arg % 2 ) == 0 ) == false; };

    {
        auto newEnd = std::remove_if( std::begin( answer1 ), std::end( answer1 ), f1_ );
        answer1.resize( std::distance( std::begin( answer1 ), newEnd ) );
    }
    {
        auto newEnd = std::remove_if( std::rbegin( answer2 ), std::rend( answer2 ), f1_ );
        answer2.erase( std::begin( answer2 ), newEnd.base( ) );
    }

    answer3.resize( answer1.size( ) );
    answer4.resize( answer2.size( ) );
    std::transform( std::begin( answer1 ), std::end( answer1 ), std::begin( answer3 ), l1 );
    std::transform( std::rbegin( answer2 ), std::rend( answer2 ), std::rbegin( answer4 ), l1 );

    filter( f1 ).map( l1 ).apply_to( v1 );
    filter( f1 ).map( l1 ).apply_reversed_to( v2 );

    test_assert( v1 == answer3 && v2 == answer4 );
}

//--------------------------------
void
    Tester::filter_fold_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1 );
    cont_t answer2( v2 );

    auto f1  = []( auto arg ) { return ( arg % 2 ) == 0; };
    auto f1_ = []( auto arg ) { return ( ( arg % 2 ) == 0 ) == false; };

    auto foldr_f = []( auto acc, auto arg ) { return acc - arg; };
    auto foldl_f = []( auto arg, auto acc ) { return arg - acc; };

    auto foldr_f_ = []( auto acc, auto arg ) { return acc - arg; };
    auto foldl_f_ = []( auto arg, auto acc ) { return acc - arg; };

    {
        auto newEnd = std::remove_if( std::begin( answer1 ), std::end( answer1 ), f1_ );
        answer1.resize( std::distance( std::begin( answer1 ), newEnd ) );
    }
    {
        auto newEnd = std::remove_if( std::rbegin( answer2 ), std::rend( answer2 ), f1_ );
        answer2.erase( std::begin( answer2 ), newEnd.base( ) );
    }

    test_assert( answer1 == answer2 );

    auto res1 = std::accumulate( std::begin( answer1 ), std::end( answer1 ), 7, foldr_f_ );
    auto res2 = std::accumulate( std::rbegin( answer2 ), std::rend( answer2 ), 7, foldl_f_ );

    auto res3 = filter( f1 ).fold( foldr_f ).apply_to( 7, v1 );
    auto res4 = filter( f1 ).fold( foldl_f ).apply_reversed_to( v2, 7 );

    test_assert( res1 == res3 && res2 == res4 );
}

//--------------------------------
void
    Tester::filter_map_filter_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1 );
    cont_t answer2( v2 );
    cont_t answer3;
    cont_t answer4;

    auto l1 = []( auto i ) { return i + 1; };

    auto f1  = []( auto arg ) { return ( arg % 2 ) == 0; };
    auto f1_ = []( auto arg ) { return ( ( arg % 2 ) == 0 ) == false; };

    {
        auto newEnd = std::remove_if( std::begin( answer1 ), std::end( answer1 ), f1_ );
        answer1.resize( std::distance( std::begin( answer1 ), newEnd ) );
    }
    {
        auto newEnd = std::remove_if( std::rbegin( answer2 ), std::rend( answer2 ), f1_ );
        answer2.erase( std::begin( answer2 ), newEnd.base( ) );
    }

    answer3.resize( answer1.size( ) );
    answer4.resize( answer2.size( ) );
    std::transform( std::begin( answer1 ), std::end( answer1 ), std::begin( answer3 ), l1 );
    std::transform( std::rbegin( answer2 ), std::rend( answer2 ), std::rbegin( answer4 ), l1 );

    {
        auto newEnd = std::remove_if( std::begin( answer3 ), std::end( answer3 ), f1_ );
        answer3.resize( std::distance( std::begin( answer3 ), newEnd ) );
    }
    {
        auto newEnd = std::remove_if( std::rbegin( answer4 ), std::rend( answer4 ), f1_ );
        answer4.erase( std::begin( answer4 ), newEnd.base( ) );
    }

    filter( f1 ).map( l1 ).filter( f1 ).apply_to( v1 );
    filter( f1 ).map( l1 ).filter( f1 ).apply_reversed_to( v2 );

    test_assert( v1 == answer3 && v2 == answer4 );
}

//--------------------------------
void
    Tester::filter_map_fold_constructed( )
{
    cont_t v1 = generate_source_data( );
    cont_t v2( v1 );
    cont_t answer1( v1 );
    cont_t answer2( v2 );
    cont_t answer3;
    cont_t answer4;

    auto l1 = []( auto i ) { return i + 1; };

    auto filter_f  = []( auto arg ) { return ( arg % 2 ) == 0; };
    auto filter_t_ = []( auto arg ) { return ( ( arg % 2 ) == 0 ) == false; };

    auto foldr_f = []( auto acc, auto arg ) { return acc - arg; };
    auto foldl_f = []( auto arg, auto acc ) { return arg - acc; };

    auto foldr_f_ = []( auto acc, auto arg ) { return acc - arg; };
    auto foldl_f_ = []( auto arg, auto acc ) { return acc - arg; };

    {
        auto newEnd = std::remove_if( std::begin( answer1 ), std::end( answer1 ), filter_t_ );
        answer1.resize( std::distance( std::begin( answer1 ), newEnd ) );
    }
    {
        auto newEnd = std::remove_if( std::rbegin( answer2 ), std::rend( answer2 ), filter_t_ );
        answer2.erase( std::begin( answer2 ), newEnd.base( ) );
    }

    answer3.resize( answer1.size( ) );
    answer4.resize( answer2.size( ) );
    std::transform( std::begin( answer1 ), std::end( answer1 ), std::begin( answer3 ), l1 );
    std::transform( std::rbegin( answer2 ), std::rend( answer2 ), std::rbegin( answer4 ), l1 );

    auto res1 = std::accumulate( std::begin( answer3 ), std::end( answer3 ), 0, foldr_f_ );
    auto res2 = std::accumulate( std::rbegin( answer4 ), std::rend( answer4 ), 0, foldl_f_ );

    auto res3 = filter( filter_f ).map( l1 ).fold( foldr_f ).apply_to( 0, v1 );
    auto res4 = filter( filter_f ).map( l1 ).fold( foldl_f ).apply_reversed_to( v2, 0 );

    test_assert( res1 == res3 && res2 == res4 );
}

namespace
{
#define D_PRINT 0
static size_t work = 0;
class data
{
  public:
    data( int i_i )
        : i( i_i )
    {
#if D_PRINT
        std::cout << " ctor,";
#endif
        work += 3;
    }

    data( )
    {
#if D_PRINT
        std::cout << " default ctor,";
#endif
        work += 3;
    }

    data( data const& other )
    {
#if D_PRINT
        std::cout << " copy ctor,";
#endif
        i = other.i;
        work += 2;
    }

    data( data&& other )
    {
#if D_PRINT
        std::cout << " move ctor,";
#endif
        std::swap( i, other.i );
        work += 1;
    }

    data&
        operator=( data const& rhs )
    {
#if D_PRINT
        std::cout << " assignment op,";
#endif
        i = rhs.i;
        work += 2;
        return *this;
    }

    data&
        operator=( data&& rhs )
    {
#if D_PRINT
        std::cout << " move assignment op,";
#endif
        std::swap( i, rhs.i );
        work += 1;
        return *this;
    }

    ~data( )
    {
#if D_PRINT
        std::cout << " dtor,";
#endif
        work += 3;
    }

    int i;
};
/* results
---- test ----
5
	work: 35
5000
	work: 6280000 <- forward
	work: 3156250 <- backward

---- test ----
5
	work: 34
5000:
	work: 35000 <- forward
	work: 37500 <- backward
*/
#undef D_PRINT
}

//--------------------------------
void
    Tester::filtering( )
{
    std::cout << "init" << std::endl;
    std::cout << "(";
    std::vector< data > v1;
    for ( size_t i = 0; i < 5000; ++i )
    {
        v1.emplace_back( static_cast< int >( i ) );
    }
    std::cout << ")" << std::endl;
    std::cout << "work: " << work << std::endl;

    auto f1 = []( auto arg ) { return ( arg.i % 2 ) == 0; };

    work = 0;
    std::cout << "---- test ----" << std::endl;
    std::cout << "(";
    filter( f1 ).apply_reversed_to( v1 );
    std::cout << ")" << std::endl;
    std::cout << "work: " << work << std::endl;
}

//--------------------------------
void
    Tester::test_all( )
{
    //filtering();

    map_simple( );
    map_constructed( );
    map_static( );
    map_manual_acc( );
    fold_manual( );
    fold_constructed( );
    fold_map_constructed( );
    filter_manual( );
    filter_constructed( );
    map_filter_constructed( );
    map_filter_map_constructed( );
    map_filter_fold_constructed( );
    filter_map_constructed( );
    filter_fold_constructed( );
    filter_map_filter_constructed( );
    filter_map_fold_constructed( );

    std::cout << "tests passed" << std::endl;
}