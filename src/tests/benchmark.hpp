/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_BENCHMARK_HPP )
#define D_BENCHMARK_HPP

#include "timer.hpp"

#include <array>
#include <random>
#include <vector>

#define D_NEW_FIRST 1

//--------------------------------
/* toUType( enumerator )
	Given an enumeration of a scoped enum, get its value.

	NOTE: borrowed from page 73 from More effective C++ by Scott Meyers (9th print)
	NOTE: C++14 version
*/
template < typename E >
constexpr auto
    toUType( E enumerator ) noexcept
{
    return static_cast< std::underlying_type_t< E > >( enumerator );
}

//--------------------------------
class Benchmark
{
    using time_t = Timer_t::timeDuration_t;

  public:
    Benchmark( size_t const i_data_count );

    void run_all( std::vector< std::string >& i_results );

  private:
    std::vector< time_t > run_new( );
    std::vector< time_t > run_std( );

    time_t map_filter_new( ) const;
    time_t map_filter_std( ) const;

    time_t map_filter_map_new( ) const;
    time_t map_filter_map_std( ) const;

    time_t map_filter_fold_new( ) const;
    time_t map_filter_fold_std( ) const;

    time_t filter_map_new( ) const;
    time_t filter_map_std( ) const;

    time_t filter_fold_new( ) const;
    time_t filter_fold_std( ) const;

    time_t filter_map_filter_new( ) const;
    time_t filter_map_filter_std( ) const;

    time_t filter_map_fold_new( ) const;
    time_t filter_map_fold_std( ) const;

    std::default_random_engine               init_generator( ) const;
    std::uniform_real_distribution< double > init_distribution( ) const;
    std::vector< double >                    init_data( ) const;

    size_t m_data_count;

    enum class eID : size_t
    {
        INVALID = static_cast< size_t >( ~0 ),

        map_filter = 0,
        map_filter_map,
        map_filter_fold,
        filter_map,
        filter_fold,
        filter_map_filter,
        filter_map_fold,

        count
    };

    std::array< const char* const, toUType( Benchmark::eID::count ) > m_testNames = {
        { "map_filter",
          "map_filter_map",
          "map_filter_fold",
          "filter_map",
          "filter_fold",
          "filter_map_filter",
          "filter_map_fold" }
    };
};

#endif