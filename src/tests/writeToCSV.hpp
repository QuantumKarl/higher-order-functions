/*	This file is part of Higher Order functions.
	Copyright(C) 2017 by Karl Wesley Hutchinson

	Higher Order functions is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Higher Order functions is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Higher Order functions. If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined( D_WRITE_TO_CSV_HPP )
#define D_WRITE_TO_CSV_HPP

#include <exception>
#include <fstream>
#include <iostream>
#include <string>

//--------------------------------
/* writeToCSV
	outputs a CSV file.

	NOTE: it expects the data to already be comma separated.
*/
void
    writeToCSV( std::vector< std::string > const& i_data, std::string const& i_fileNameStr )
{
    try
    {
        //---- create a file handle ----
        std::ofstream outfile( i_fileNameStr + ".csv" ); // default mode overwrites data

        //---- write the data to file ----
        for ( auto& e : i_data )
        {
            outfile << e << '\n';
        }

        outfile.flush( );
        outfile.close( );
    }
    catch ( std::exception& e )
    {
        std::cout << "FAILED to write output file with exception: \n" << e.what( ) << std::endl;
    }
}

#endif
