# Higher Order functions

This small project is an experiment to see if you can use a chain of higher order functions in a single inner loop. For example calling std::map then std::filter would cause two separate loops.
This project avoids that second loop and simples the syntax without losing any functionality that std has.
Look at src\tests.cpp for functioning code.

## Does it make any difference?
Yes! Benchmarks have shown the "new" (this project in blue) high order functions perform around 39% faster than (std library in red):

![performance graph](https://bytebucket.org/QuantumKarl/higher-order-functions/raw/15d277c32af7fed86dd3060a8d4f3d150883eb7e/doc/Win32Performance.png)

See the doc folder.

## Getting Started

You can build the project via make or Visual Studio 2015.

### Prerequisites

* C++14 compiler.

## Running the tests

The tests automatically run as part of the main program.

## Running the benchmark

The benchmark runs automatically after the tests. To change the test size see constant at the top of src\main.cpp.

## Deployment

If you would like to use this project simply take the contents of the "higher_order" folder and include "higher_order.hpp".

## License

GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
 
## Future tasks
* More performance tests with non-pod data types.

* Run performance tests on linux.
* Run code coverage tests (memory tests already passed).
* Run performance tests on linux
